module.exports = function(db) {
    var cake_producto={};

    cake_producto.getAllProductos = function(callback) {
        console.log('getAllKegetAllProductosyValues', arguments);

        db.getConnection(function(err, connection){
            connection.query(
                " SELECT * " +
                " FROM productos",[],
                function(err, rows) {

                    if (err){
                        callback(err);
                    }
                    else{
                        console.log('getAllProductos result',rows);
                        callback(rows || {});
                    }
                });
            connection.release();
        });
    };

    cake_producto.addNewKeyValue = function(client_id, name, callback) {
        try{
            console.log('addNewKeyValue ', arguments)

            db.getConnection(function(err, connection){
                connection.query(
                    ' INSERT INTO sl_client (client_id , name) VALUES (?, ?) ',[client_id, name],
                    function(err, rows) {

                        if (err){
                            callback(err);
                        }
                        else{
                            console.log('addNewKeyValue',rows[0]);
                            callback(rows[0] || {});
                        }
                    });
                connection.release();
            });
        }catch (e){
            //console.warn(e);
        }
    };

    cake_producto.updateEntryValue = function(client_id, name, callback) {
        console.log('updateEntryValue ', arguments)

        db.getConnection(function(err, connection){
            connection.query(
                ' UPDATE sl_client SET ' +
                ' name = ? ' +
                ' WHERE client_id = ? ', [name, client_id],

                function(err, rows) {

                    if (err){
                        callback(err);
                    }
                    else{
                        console.log('updateEntryValue',rows[0]);
                        callback(rows[0] || {});
                    }
                });
            connection.release();
        });
    };
    return cake_producto;
};

