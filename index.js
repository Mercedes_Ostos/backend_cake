var express = require("express");
var mysql = require('mysql');
var app = express();
//app.use(express.logger());
var request = require('request');
var useragent = require('express-useragent');
app.use(useragent.express());

var db_config = {
    host: 'us-cdbr-iron-east-01.cleardb.net',
    user: 'ba9b720158e2aa',
    password: 'e6f37b24',
    database: 'heroku_c900ef11e62636c'
};
var connection;
var pool = mysql.createPool(db_config);
var session = require('express-session');
var cookieParser = require('cookie-parser');
var SessionStore = require('express-mysql-session');
var sessionConnection = mysql.createConnection(db_config);
var sessionStore = new SessionStore({}, sessionConnection );
var http = require('http');
var hostname = '127.0.0.1';
var cakeProducto = require('./cake_producto.js')(pool);
var cakeProveedores = require('./cake_proveedores.js')(pool);
var cakeCategorias = require('./cake_categorias.js')(pool);
var cakeEnvios = require('./cake_envios.js')(pool);

function handleDisconnect() {
    console.log('1. connecting to db:');
    connection = mysql.createConnection(db_config); // Recreate the connection, since
													// the old one cannot be reused.

    connection.connect(function(err) {              	// The server is either down
        if (err) {                                     // or restarting (takes a while sometimes).
            console.log('2. error when connecting to db:', err);
            setTimeout(handleDisconnect, 1000); // We introduce a delay before attempting to reconnect,
        }                                     	// to avoid a hot loop, and to allow our node script to
    });                                     	// process asynchronous requests in the meantime.
    											// If you're also serving http, display a 503 error.
    connection.on('error', function(err) {
        console.log('3. db error', err);
        if (err.code === 'PROTOCOL_CONNECTION_LOST') { 	// Connection to the MySQL server is usually
            handleDisconnect();                      	// lost due to either server restart, or a
        } else {                                      	// connnection idle timeout (the wait_timeout
            throw err;                                  // server variable configures this)
        }
    });
}

handleDisconnect();

app.get('/productos_cake', function(req, res){
	cakeProducto.getAllProductos(function(data){
		res.send(data);
	})
});

app.get('/proveedores_cake', function(req, res){
	cakeProveedores.getAllProveedores(function(data){
		res.send(data);
	})
});

app.get('/categorias_cake', function(req, res){
	cakeCategorias.getAllCategorias(function(data){
		res.send(data);
	})
});

app.get('/envios_cake', function(req, res){
	cakeEnvios.getAllEnvios(function(data){
		res.send(data);
	})
});

var port = process.env.PORT || 5000;
app.listen(port, hostname,function() {
    console.log("Listening on " + port);
    console.log(`Server running at https://${hostname}:${port}/`);
});